# Drop dokuwiki template

* Based on a wordpress theme
* Designed by [Compete Themes](https://wordpress.org/themes/drop/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:drop)

![drop theme screenshot](https://i.imgur.com/4hUHbxn.png)