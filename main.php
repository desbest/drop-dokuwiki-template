<?php
/**
 * DokuWiki Drop Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:drop
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <link rel="stylesheet" id="ct-drop-google-fonts-css" href="//fonts.googleapis.com/css?family=Montserrat%3A400%2C700%7COpen+Sans%3A400italic%2C400%2C700&amp;ver=5.4.4" type="text/css" media="all">
</head>

<body id="drop">

 <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>

<div class='overflow-container <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>' id="dokuwiki__top">
    <a class="skip-content" href="#main">Skip to content</a>
<?php tpl_includeFile('header.html') ?>
<header id="site-header" class="site-header" role="banner">

    <div itemscope itemtype="http://schema.org/Organization" class="title-info">
        <h1 class="site-title" itemprop="name"><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
        <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img class="logo" itemprop="logo" src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
    </div>
    
    <?php //get_template_part( 'menu', 'primary' ); // adds the primary menu ?>
    <button id="toggle-button" class="toggle-button">
    <i class="fa fa-bars"></i>
    </button>

    <div class='menu-slider'>
        <div class='site-info'>
            <?php if ($conf['tagline']): ?>
                <p class="claim"><?php echo $conf['tagline'] ?></p>
            <?php endif ?>
            <div class="searchcontainer"><?php tpl_searchform() ?></div>
             <?php
                if (!empty($_SERVER['REMOTE_USER'])) {
                    //echo '<li class="user">';
                    tpl_userinfo(); /* 'Logged in as ...' */
                    //echo '</li>';
                }
            ?>
            <!-- <br><br> -->
            <!-- <hr /> -->
        </div>

      

        <div class="menu-container menu-primary" role="navigation">
            <div class="menu-container menu-primary" role="navigation">

                   <!-- ********** ASIDE ********** -->
        <?php if ($showSidebar): ?>
            <div id="writtensidebar">
                <?php tpl_includeFile('sidebarheader.html') ?>
                <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                <?php tpl_includeFile('sidebarfooter.html') ?>
                <div class="clearer"></div>
                <hr>
            </div>
        <?php endif; ?>

            <ul id="menu-primary-items" class="menu-primary-items" role="menubar">
            <!-- SITE TOOLS -->
                <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                
                    <?php tpl_toolsevent('sitetools', array(
                        'recent'    => tpl_action('recent', 1, 'li class=\'menu-item \'', 1),
                        'media'     => tpl_action('media', 1,  'li class=\'menu-item \'', 1),
                        'index'     => tpl_action('index', 1,  'li class=\'menu-item \'', 1),
                    )); ?>

                    <!-- USER TOOLS -->
                <?php if ($conf['useacl'] && $showTools): ?>
                    <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                    <!-- <ul> -->
                       
                        <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                                 e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                        ?>
                        <?php tpl_toolsevent('usertools', array(
                            'admin'     => tpl_action('admin', 1, 'li', 1),
                            'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                            'profile'   => tpl_action('profile', 1, 'li', 1),
                            'register'  => tpl_action('register', 1, 'li', 1),
                            'login'     => tpl_action('login', 1, 'li', 1),
                        )); ?>
                    <!-- </ul> -->
                <?php endif ?>

            </ul></div>
        </div><!-- #menu-primary .menu-container -->
        <?php //ct_drop_social_media_icons(); // adds social media icons ?>
    </div>

</header>
<div id="main" class="main" role="main">

    <div class="entry" itemscope="" itemtype="http://schema.org/BlogPosting">
        <!-- <div itemprop="thumbnailUrl" class="featured-image" style="background-image: url('http://localhost/wordpress/wp-content/uploads/2020/02/world-cup-cake-meme.jpg')"></div>         -->
        
        <div class="excerpt-meta">


            <!-- PAGE ACTIONS -->
            <?php if ($showTools): ?>
                <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                <ul class="newmetamenu">
                    <?php tpl_toolsevent('pagetools', array(
                        'edit'      => tpl_action('edit', 1, 'li', 1),
                        'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                        'revisions' => tpl_action('revisions', 1, 'li', 1),
                        'backlink'  => tpl_action('backlink', 1, 'li', 1),
                        'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                        'revert'    => tpl_action('revert', 1, 'li', 1),
                        'top'       => tpl_action('top', 1, 'li', 1),
                    )); ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="entry-meta-top">
            <!-- BREADCRUMBS -->
            <?php if($conf['breadcrumbs']){ ?>
                <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
            <?php } ?>
            <?php if($conf['youarehere']){ ?>
                <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
            <?php } ?>

            <div class="dokuwikierrorsandnotices"><?php html_msgarea() /* occasional error and info messages on top of the page */ ?></div>
        </div>        
        <!-- <div class="entry-header">
            <h1 class="entry-title" itemprop="headline">Test post</h1>
        </div> -->
        <div class="entry-content">
            <article itemprop="articleBody">
                <?php tpl_flush() /* flush the output buffer */ ?>
                <?php tpl_includeFile('pageheader.html') ?>

                <div class="page">
                    <!-- wikipage start -->
                    <?php tpl_content() /* the main content */ ?>
                    <!-- wikipage stop -->
                    <div class="clearer"></div>
                </div>

                <?php tpl_flush() ?>
                <?php tpl_includeFile('pagefooter.html') ?>
                                            </article>
        </div>
        <div class="entry-meta-bottom">
            <ul class="a11y skip">
                    <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
            </ul>

            <!-- <nav class="further-reading"><p class="prev">
                <span>Previous Post</span>
                <a href="http://localhost/wordpress/hello-world/">Hello world!</a>
            </p><p class="next">
                <span>This is the newest post</span>
                <a href="http://localhost/wordpress">Return to Blog</a>
             </p></nav>         <div class="entry-categories"><p><span>Categories:</span><a href="http://localhost/wordpress/category/uncategorized/" title="View all posts in Uncategorized">Uncategorized</a></p></div>
            <div class="entry-tags"></div> -->
        </div>
    </div>

    <section id="comments" class="comments">
    </section>

</div> <!-- .main -->

<footer class="site-footer" role="contentinfo">
    <?php //get_sidebar( 'subsidiary' ); ?>
    <div class="copyright">
        <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
            <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
        <p>Designed by <a href="http://www.competethemes.com">Compete Themes</a></p>
        <?php tpl_includeFile('footer.html') ?>
    </div>
</footer>
 

       

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
  
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/functions.js"></script>
    <!-- due to the way dokuwiki buffers output, this javascript has to
            be before the </body> tag and not in the <head> -->
</body>
</html>
